import numpy as np
import matplotlib.pyplot as plt
import random
from scipy.stats import norm

def print_error(bin_edges, Bins_Test):

    err = 0
    int_p = [0, 0.025, 0.16, 0.84, 0.975, 1]

    for i in range(5):

        p_interval = int_p[i+1] - int_p[i]
        err += (Bins_Test[i]-p_interval)**2

    err /= 5

    print("Aggregated error:", "%.8f" % err)


if __name__ == "__main__":

    m = 20000
    mu = 5
    sigma = np.sqrt(100/(12*50))

    Y = np.loadtxt('p11.txt')

    Bins_Test = np.zeros(5)

    bin_edges = np.zeros(4)
    bin_edges[0] = norm.ppf(0.025) * sigma + mu
    bin_edges[1] = norm.ppf(0.16) * sigma + mu
    bin_edges[2] = norm.ppf(0.84) * sigma + mu
    bin_edges[3] = norm.ppf(0.975) * sigma + mu


    for i in range(len(Y)):
        if Y[i] <= bin_edges[0]:
            Bins_Test[0] += 1
        elif bin_edges[0] < Y[i] <= bin_edges[1]:
            Bins_Test[1] += 1
        elif bin_edges[1] < Y[i] <= bin_edges[2]:
            Bins_Test[2] += 1
        elif bin_edges[2] < Y[i] <= bin_edges[3]:
            Bins_Test[3] += 1
        else:
            Bins_Test[4] += 1

    Bins_Test = Bins_Test / np.sum(Bins_Test)

    print_error(bin_edges, Bins_Test)


