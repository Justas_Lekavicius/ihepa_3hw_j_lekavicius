import numpy as np
import matplotlib.pyplot as plt
import random

def Produce_rv(a, b, n):
    Y = 0

    for j in range(n):
        Y += random.uniform(a, b)

    return Y/n


if __name__ == "__main__":

    a1 = 0
    b1 = 10
    a2 = -2
    b2 = 16
    n1 = 50
    n2 = n1
    m = 50000
    Y = np.zeros(m)
    A = np.zeros(m)

    for i in range(m):
        Y[i] = Produce_rv(a1, b1, n1)
        A[i] = Produce_rv(a2, b2, n2)

    TS_obs_vector = np.full(50, 6)
    TS_obs = np.sum(TS_obs_vector) / 50.0

    #Producing p_A value
    p_A = 0.0

    for i in range(m):
        if (A[i] <= TS_obs):
            p_A += 1

    p_A = p_A / m

    print("p value:", "%.4f" % p_A)


    #I am exporting this because I want to work with identical distributions in other parts of the problem
    np.savetxt('p21.txt', np.column_stack((Y, A)), fmt='%.6f')



