import numpy as np
import matplotlib.pyplot as plt
import random

def plot_hist(dist, bins, title, name, x_label, y_label):
    plt.hist(dist, bins=bins, color="gray")
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.title(title)

    name_png = name + ".png"
    name_pdf = name + ".pdf"
    plt.savefig(name_png)
    plt.savefig(name_pdf)

    plt.close()

def Produce_y_i(a, b, n):
    Y = 0

    for j in range(n):
        Y += random.uniform(a, b)

    return Y/n

if __name__ == "__main__":

    a = 0
    b = 10
    n = 50
    m = 20000
    Y = np.zeros(m)

    for i in range(m):
        Y[i] = Produce_y_i(a, b, n)

    #Google suggests to use sqrt(m) as a bin number (the easiest approach)
    bins = np.sqrt(m)
    bins = bins.astype(int)

    plot_hist(Y, bins, "Proof of CLT", "p11", "Y values", "Counts")
    np.savetxt('p11.txt', Y, fmt='%.4f')


