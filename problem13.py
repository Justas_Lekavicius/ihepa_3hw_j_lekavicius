import numpy as np
import matplotlib.pyplot as plt
import random

def First_method(Y, X_obs, n):

    p = 0.0

    for i in range(n):
        if (Y[i] >= X_obs):  #In this method I basically treat each x axis value as a bin
            p += 1

    p = p / n

    error = np.sqrt(p*(1-p)/n) * 100 #This formula is selected because p value is a binomial-ish probability distribution

    print("p value:", "%.4f" % p)
    print("error value (%):", "%.4f" % error)


def Second_method(X_pmf, Y_pmf, X_obs, n):

    p_2 = 0

    for i in range(len(X_pmf)):
        if X_pmf[i] >= X_obs:
            p_2 += Y_pmf[i]

    error_2 = np.sqrt(p_2*(1-p_2)/len(X_pmf)) * 100

    print("\nMore staightforward calculation yields a similar p value (at least I hope so) with higher error:")
    print("p value:", "%.4f" % p_2)
    print("error value (%):", "%.4f" % error_2)



if __name__ == "__main__":

    #The last part of the problem did not mention any accuracies I had to achieve, so I decided to work with bins instead of an array with 20k numbers. I could use the previous approach here but this results in higher p value error than I want (because I would have to use number of bins instead of n=20k when calculating the error).

    Y = np.loadtxt('p11.txt')
    n = len(Y)

    X_obs_vector = np.full(50, 6) #this was not neccesary...
    X_obs = np.sum(X_obs_vector) / 50.0

    First_method(Y, X_obs, n)

    #If I have followed my previous approach (working with bins), the p value would be approximately the same but have a higher error

    data = np.loadtxt('p12.txt')
    X_pmf = data[:,0]
    Y_pmf = data[:,1]

    Second_method(X_pmf, Y_pmf, X_obs, n)


