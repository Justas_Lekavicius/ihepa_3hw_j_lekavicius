import numpy as np
import matplotlib.pyplot as plt
import random

def produce_pmf(Y):

    m = len(Y)
    Y_pmf, edges = np.histogram(Y, bins=(np.sqrt(m)).astype(int))

    Y_pmf = Y_pmf.astype(float)
    Y_pmf /= np.sum(Y_pmf)

    X_pmf = (edges[1:] + edges[:-1]) / 2.0

    return X_pmf, Y_pmf


if __name__ == "__main__":

    Y = np.loadtxt('p11.txt')
    X_pmf, Y_pmf = produce_pmf(Y)

    #Calculating CIs
    SUM = 0
    CI_68 = np.zeros(2)
    CI_95 = np.zeros(2)
    CI_99 = np.zeros(2)

    #According to wikipedia CI is defined as P(CI_low < X < CI_high) = p
    #I will use this definition. The sign (< of <=) does not have much impact, because if I use the following definition P(CI_low <= X <= CI_high) = p, the CI values vary only in the second decimal place (I have done some testing)

    for i in range(len(Y_pmf)):
        SUM += Y_pmf[i]

        if(SUM > 0.16 and CI_68[0] == 0):
            CI_68[0] = X_pmf[i]

        if(SUM > 0.84 and CI_68[1] == 0):
            CI_68[1] = X_pmf[i-1] #I purposely go over the line and then use (i-1)th element

        if(SUM > 0.025 and CI_95[0] == 0):
            CI_95[0] = X_pmf[i]

        if(SUM > 0.975 and CI_95[1] == 0):
            CI_95[1] = X_pmf[i-1]

        if(SUM > 0.005 and CI_99[0] == 0):
            CI_99[0] = X_pmf[i]

        if(SUM > 0.995 and CI_99[1] == 0):
            CI_99[1] = X_pmf[i-1]


    #Constructing a fancy histogram
    counter_for_legend = np.zeros(4)
    bin_width = X_pmf[1]-X_pmf[0]

    for i in range(len(X_pmf)):
        if CI_68[0] < X_pmf[i] <= CI_68[1]: #Here I use <= instead of < to account for all possible numbers. This is not so important because I am doing it only for the purpose of graphical representation
            color = 'green'
            if (counter_for_legend[0] == 0): #I must add a label for each category only once
                plt.bar(X_pmf[i], Y_pmf[i], width=bin_width, color=color, label="68% CI")
                counter_for_legend[0] = 1

        elif CI_95[0] < X_pmf[i] <= CI_68[0] or CI_68[1] < X_pmf[i] <= CI_95[1]:
            color = 'yellow'
            if (counter_for_legend[1] == 0):
                plt.bar(X_pmf[i], Y_pmf[i], width=bin_width, color=color, label="95% CI")
                counter_for_legend[1] = 1

        elif CI_99[0] < X_pmf[i] <= CI_95[0] or CI_95[1] < X_pmf[i] <= CI_99[1]:
            color = 'orange'
            if (counter_for_legend[2] == 0):
                plt.bar(X_pmf[i], Y_pmf[i], width=bin_width, color=color, label="99% CI")
                counter_for_legend[2] = 1

        else:
            color = 'red'
            if (counter_for_legend[3] == 0):
                plt.bar(X_pmf[i], Y_pmf[i], width=bin_width, color=color, label="The rest")
                counter_for_legend[3] = 1

        plt.bar(X_pmf[i], Y_pmf[i], width=bin_width, color=color)


    plt.xlabel("Y values")
    plt.ylabel("PMF")
    plt.title("Histogram with confidence intervals")
    plt.legend(loc='upper right', fontsize='large', facecolor='lightgray')
    plt.savefig("p12.png")
    plt.savefig("p12.pdf")
    plt.close()

    print("68% confidence interval: ", ["%.2f" % num for num in CI_68])
    print("95% confidence interval: ", ["%.2f" % num for num in CI_95])
    print("99% confidence interval: ", ["%.2f" % num for num in CI_99])


    #I am exporting this because I want to work with identical distribution in other problems
    np.savetxt('p12.txt', np.column_stack((X_pmf, Y_pmf)), fmt='%.6f')
