import numpy as np
import matplotlib.pyplot as plt
import random
import math

if __name__ == "__main__":

    data = np.loadtxt('p21.txt')
    Y = data[:,0]
    A = data[:,1]
    m = len(Y)

    TS_obs_vector = np.full(50, 6)
    TS_obs = np.sum(TS_obs_vector) / 50.0

    #Producing alpha - signal is H_O (aka Y distribution), so I will use it for alpha

    alpha = 0.0

    for i in range(m):
        if (Y[i] >= TS_obs):
            alpha += 1

    alpha /= m

    #Producing beta

    beta = 0.0

    for i in range(m):
        if (A[i] <= TS_obs):
            beta += 1

    beta /= m

    CLS = alpha / (1-beta)

    print("CLS value:", "%.4f" % CLS)
